import { createConnection, getRepository } from "typeorm"
import * as request from "supertest"
import app from "../../src/app"
import { port } from "../../src/config"
import { User } from "../../src/entity/User"

let connection, server

const testUser = {
  firstName: "John",
  lastName: "Doe",
  age: 20
}

beforeEach(async() => {
  connection = await createConnection()
  //await connection.synchronize(true)
  server = app.listen(port)
})

afterEach(() => {
  connection.close()
  server.close()
})


it("find all users", async() => {
  const response = await request(app).get("/users")
  expect(response.statusCode).toBe(200)
})

it("find one users", async() => {
  const response = await request(app).get("/users/1")
  expect(response.statusCode).toBe(200)
})

it("create user", async() => {
  const response = await request(app).post("/users").send(testUser)
  expect(response.statusCode).toBe(200)
})

it("should not create a user if no firstName is given", async() => {
  const response = await request(app).post("/users").send({ lastName: "Doe", age: 21 })
  expect(response.statusCode).toBe(400)
  expect(response.body.errors).not.toBeNull()
  expect(response.body.errors.length).toBe(1)
  expect(response.body.errors[0]).toEqual({
    msg: "Invalid value", param: "firstName", location: "body"
  })
})

it("should not create a user if age is less than 0", async() => {
  const userFind = getRepository(User)
  const response = await request(app).delete(`/users/${(await userFind.findOne()).id}`)
  expect(response.statusCode).toBe(200)
})
