import * as express from "express"
import { Request, Response } from "express"
import * as morgan from "morgan"
import { Routes } from "./routes"
import { validationResult } from "express-validator"
import * as cors from "cors"
import { ErrorHandler } from "./middlewares/ErrorHandler"

/* eslint-disable */
const app = express()
app.use(morgan("tiny"))
app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(cors())

Routes.forEach(route => {
  (app as any)[route.method](route.route,
    ...route.validation,
    async (req: Request, res: Response, next: Function) => {

    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
      }

      const result = await (new (route.controller as any))[route.action](req, res, next)
      res.json(result)
    } catch(err) {
      next(err)
    }
  })
})

app.use(ErrorHandler)

app.all('*', (req: Request, res: Response) => {
  res.status(404).json({ error: "Ruta no encontrada" })
})

export default app
