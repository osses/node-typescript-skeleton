import { Request, Response, NextFunction } from "express"
import Logger from "../libs/logger"

/* eslint-disable */
export function ErrorHandler(err:any, _req:Request, res:Response, _next: NextFunction) { 
    Logger.error(err.message)
    res.status(err.statusCode || 500).send({ error: "Internal Server Error" })
  }