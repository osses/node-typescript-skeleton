import { getRepository } from "typeorm"
import { Request, Response } from "express"
import { User } from "../entity/User"


/* eslint-disable */
import Logger from "../libs/logger"
export class UserController {

  private userRepository = getRepository(User)

  async all(request: Request, response: Response) {
    Logger.info("Getting all users")
    return this.userRepository.find()
  }

  async one(request: Request) {
    return this.userRepository.findOne(request.params.id)
  }

  async save(request: Request) {
    return this.userRepository.save(request.body)
  }

  async remove(request: Request) {
    const userToRemove = await this.userRepository.findOne(request.params.id)
    await this.userRepository.remove(userToRemove)
  }
}
