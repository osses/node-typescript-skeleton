## Prerequisites

- nodeJS

# Base dependencies

````
"axios": "^0.26.0",
"cors": "^2.8.5",
"dotenv": "^10.0.0",
"express": "^4.17.3",
"express-validator": "^6.13.0",
"morgan": "^1.10.0",
"pg": "^8.4.0",
"reflect-metadata": "^0.1.10",
"typeorm": "0.2.40",
"winston": "^3.3.0",
"winston-daily-rotate-file": "^4.6.1"
````

# Dev dependencies

````
"@types/cors": "^2.8.12",
"@types/jest": "^27.0.3",
"@types/node": "^8.0.29",
"@typescript-eslint/eslint-plugin": "^5.10.1",
"@typescript-eslint/parser": "^5.10.1",
"eslint": "^8.8.0",
"jest": "^27.4.3",
"jest-cli": "^27.4.7",
"nodemon": "^2.0.15",
"supertest": "^6.1.6",
"ts-jest": "^27.1.0",
"ts-node": "3.3.0",
"typescript": "^3.3.3333"
````
## Running the Project

1. Run `npm i` command
2. Run `npm run dev` command development
3. Run `npm run build` command production
4. Run `npm start` command
5. Run `npm run test:coverage` command test and coverage

## Example Running test:coverage
![Screenshot](coverage.jpg)
## Docker with postgress and adminer

1. Run `docker-compose up` command

## wiki


https://github.com/validatorjs/validator.js 
https://github.com/expressjs/morgan
https://github.com/winstonjs/winston
https://github.com/expressjs/cors
https://typeorm.io/#/