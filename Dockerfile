FROM node:17.1.0-alpine
RUN npm install -g nodemon
RUN npm install -g ts-node

# work direction
WORKDIR /app
COPY ./package*.json ./

# access to work dir
RUN cd /app/

RUN chmod -R 777 /root

# install packages
RUN npm install

CMD ["npm", "run", "dev"]